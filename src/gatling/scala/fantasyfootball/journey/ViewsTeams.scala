package fantasyfootball.journey

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import fantasyfootball.request._
import scala.concurrent.duration._

object ViewsTeams {
  private val userFeed = csv("users_viewteams.csv").circular

  def actions(): ChainBuilder = {
    feed(userFeed)
      .exec(GetGameState.request)
      .pause(10 seconds, 20 seconds)
      .exec(Login.request)
      .pause(1 seconds, 2 seconds)
      .exec(GetTeamList.request)
      .pause(5 seconds, 60 seconds)
      .exec(GetTeam.request)
      .pause(10 seconds, 50 seconds)
      .exec(GetTeamList.request)
      .pause(1 seconds, 5 seconds)
      .exec(GetTeam.request)
      .pause(10 seconds, 50 seconds)
      .exec(GetLeaderboard.request)
  }
}
