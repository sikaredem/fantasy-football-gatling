package fantasyfootball.journey

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import fantasyfootball.request._
import scala.concurrent.duration._

object MakesTransfer {
  private val usersFeed = csv("users_maketransfer.csv").circular
  private val transfersFeed = csv("transfers.csv").random

  def actions(): ChainBuilder = {
    exec(GetGameState.request)
      .feed(usersFeed)
      .feed(transfersFeed)
      .pause(20 seconds, 40 seconds)
      .exec(Login.request)
      .pause(1 seconds, 2 seconds)
      .exec(GetTeamList.request)
      .pause(5 seconds, 60 seconds)
      .exec(GetTeam.request)
      .pause(10 seconds, 120 seconds)
      .exec(MakeTransfer.request)
      .pause(5 seconds, 10 seconds)
      .exec(GetTeam.request)
  }
}