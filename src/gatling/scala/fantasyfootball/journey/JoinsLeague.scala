package fantasyfootball.journey

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import fantasyfootball.request._
import scala.concurrent.duration._

object JoinsLeague {
  private val teamsFeed = csv("teams.csv").random
  private val leaguesFeed = csv("leagues.csv").random
  private val usersFeed = csv("users_joinleague.csv").circular

  def actions(): ChainBuilder =
    exec(GetGameState.request)
      .pause(10 seconds, 20 seconds)
      .feed(usersFeed)
      .feed(leaguesFeed)
      .feed(teamsFeed)
      .exec(Login.request)
      .pause(1 seconds)
      .exec(GetTeamListNoSave.request)
      .pause(100 seconds, 200 seconds)
      .exec(CreateTeam.request)
      .pause(2 seconds)
      .exec(GetTeam.request)
      .pause(5 seconds, 30 seconds)
      .exec(GetLeagueList.request)
      .pause(5 seconds, 10 seconds)
      .exec(GetLeagueList.request)
      .exec(JoinLeague.request)
}