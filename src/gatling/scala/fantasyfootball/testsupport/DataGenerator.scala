package fantasyfootball.testsupport

import java.util.UUID

/**
  * Utilities for generating data
  */
object DataGenerator {
  def randString(): String = {
    UUID.randomUUID().toString.substring(0, 8)
  }
}
