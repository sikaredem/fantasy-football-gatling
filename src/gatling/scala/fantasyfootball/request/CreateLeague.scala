package fantasyfootball.request

import fantasyfootball.testsupport.DataGenerator
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object CreateLeague {
  val request = http("setcreateleague")
    .get("/api/setcreateleague")
    .queryParamMap(Map(
      "lName" -> DataGenerator.randString(),
      "tPin" -> "${teamPin}",
      "dur" -> "S",
      "period" -> "1"
    ))
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
    .check(jsonPath("$.lPin").saveAs("leaguePin"))
}
