package fantasyfootball.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object GetLeaderboard {
  val request = http("getleaderboard")
    .get("/api/getleaderboard")
    .queryParam("type", "S")
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
}
