package fantasyfootball.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object MakeTransfer {
  val request = http("setteamupdate")
    .get("/api/setteamupdate")
    .queryParamMap(Map(
      "tPin" -> "${teamPin}",
      "tString" -> "${transfer}",
      "captain" -> 4213
    ))
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
}
