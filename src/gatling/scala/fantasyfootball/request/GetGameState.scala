package fantasyfootball.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object GetGameState {
  val request = http("getgamestate")
    .get("/api/getgamestate")
    .header("Accept", "application/json")
    .check(jsonPath("$.success").is("true"))
}
