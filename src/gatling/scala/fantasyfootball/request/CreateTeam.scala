package fantasyfootball.request

import fantasyfootball.testsupport.DataGenerator
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object CreateTeam {
  val request = http("createteam")
    .get("/api/setteamnew")
    .queryParamMap(Map(
      "tString" -> "${players}",
      "tSupp" -> "",
      "captain" -> "${captain}",
      "tName" -> DataGenerator.randString()
    ))
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
    .check(jsonPath("$.tPin").saveAs("teamPin"))
}
