package fantasyfootball.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object GetTeamListNoSave {
  val request = http("getteamlistnosave")
    .get("/api/getteamlist")
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
}
