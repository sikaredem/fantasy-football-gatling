package fantasyfootball.request

import fantasyfootball.Config
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Performs the login using either
  * 1. The skybet sso stub (node app that mocks out the sky sso server)
  * 2. The api backdoor (used to test the app in isolation)
  */
object Login {
  val request = if (Config.appConfig.getBoolean("useSsoStub")) {
    http("loginSSOStub")
      .get("/api/login")
      .header("Cookie", "SKFFToken=${SKFFToken}")
      .check(jsonPath("$.success").is("true"))
      .check(jsonPath("$.sKey").saveAs("SKFFSession"))
  } else {
    http("login")
      .get("/api/login")
      .queryParam("mPin", "${managerPin}")
      .check(jsonPath("$.success").is("true"))
      .check(jsonPath("$.sKey").saveAs("SKFFSession"))
  }
}
