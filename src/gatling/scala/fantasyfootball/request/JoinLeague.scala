package fantasyfootball.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object JoinLeague {
  val request = http("joinleague")
    .get("/api/setjoinleague")
    .queryParamMap(Map(
      "lPin" -> "${publicLeaguePin}",
      "tPin" -> "${teamPin}"
    ))
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
}
