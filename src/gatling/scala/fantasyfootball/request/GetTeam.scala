package fantasyfootball.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object GetTeam {
  val request = http("getteam")
    .get("/api/getteam")
    .queryParam("tPin", "${teamPin}")
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
}
