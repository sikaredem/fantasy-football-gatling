package fantasyfootball.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object GetTeamList {
  val request = http("getteamlist")
    .get("/api/getteamlist")
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
    .check(jsonPath("$.teams[0].tPin").saveAs("teamPin"))
}
