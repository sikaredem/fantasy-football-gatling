package fantasyfootball.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object GetLeagueList {
  val request = http("getleaguelist")
    .get("/api/getleaguelist")
    .header("Cookie", "${SKFFSession}")
    .check(jsonPath("$.success").is("true"))
}
