package fantasyfootball.simulation

import fantasyfootball.journey.{JoinsLeague, MakesTransfer, ViewsTeams}
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

import scala.concurrent.duration._

/**
  * Throttles requests to simulate the expected load of the system
  */
class ExpectedLoadTest extends Simulation {
  val throttle = Array(reachRps(25) in (3 minutes), holdFor(1 hour)) // 75 rps in total

  setUp(
    scenario("User View Scenario").exec(repeat(4) {
      ViewsTeams.actions()
    }).inject(constantUsersPerSec(5) during (800 seconds)).throttle(throttle: _*),

    scenario("Join League Scenario").exec(repeat(2) {
      JoinsLeague.actions()
    }).inject(constantUsersPerSec(5) during (1600 seconds)).throttle(throttle: _*),

    scenario("Make Transfer Scenario").exec(repeat(4) {
      MakesTransfer.actions()
    }).inject(constantUsersPerSec(5) during (800 seconds)).throttle(throttle: _*)
  ).protocols(Http.protocol).maxDuration(1 hour)
}

/**
  * Throttles requests to simulate twice the expected load of the system
  */
class TwiceExpectedLoadTest extends ExpectedLoadTest {
  override val throttle = Array(reachRps(50) in (3 minutes), holdFor(1 hour)) // 150 rps in total
}
