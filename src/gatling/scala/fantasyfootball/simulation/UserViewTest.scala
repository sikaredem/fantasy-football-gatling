package fantasyfootball.simulation

import fantasyfootball.journey.ViewsTeams
import io.gatling.core.Predef._

import scala.concurrent.duration._

class UserViewTest extends Simulation {
  val scn = scenario("User View Scenario")
    .exec(ViewsTeams.actions())
    .inject(constantUsersPerSec(1) during (500 seconds))

  setUp(scn).protocols(Http.protocol)
}

class ValidateUserViewTest extends Simulation {
  val scn = scenario("Validate User View Scenario")
    .exec(ViewsTeams.actions())
    .inject(atOnceUsers(1))
    .disablePauses

  setUp(scn).protocols(Http.protocol)
}
