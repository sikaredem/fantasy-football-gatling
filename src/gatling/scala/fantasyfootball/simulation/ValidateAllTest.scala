package fantasyfootball.simulation

import fantasyfootball.journey.{JoinsLeague, MakesTransfer, ViewsTeams}
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

/**
  * Validates that the steps in all tests can be run.
  */
class ValidateAllTest extends Simulation {
  val constantUsers = atOnceUsers(1)

  setUp(
    scenario("User View Scenario").exec(ViewsTeams.actions()).inject(constantUsers).disablePauses,
    scenario("Join League Scenario").exec(JoinsLeague.actions()).inject(constantUsers).disablePauses,
    scenario("Make Transfer Scenario").exec(MakesTransfer.actions()).inject(constantUsers).disablePauses
  ).protocols(Http.protocol)
}
