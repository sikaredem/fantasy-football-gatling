package fantasyfootball.simulation

import fantasyfootball.Config
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Http {
  private val apiUrl = Config.appConfig.getString("apiUrl")

  val protocol = http.baseURL(apiUrl)
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0")
}
