package fantasyfootball.simulation

import fantasyfootball.journey.{JoinsLeague, MakesTransfer, ViewsTeams}
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

import scala.concurrent.duration._

/**
  * Scenario to simulate a spike in traffic that is approx. ten times the expected load (75 rps)
  */
abstract class SpikeTest extends Simulation {
  val constantUsers = constantUsersPerSec(5) during (240 seconds)
  val spikeInjectionSpec = List(nothingFor(2 minutes), atOnceUsers(200))
  val spikeThrottle = Array(reachRps(750) in (10 seconds), holdFor(1 minute))

  setUp(
    // load
    scenario("User View Scenario").exec(ViewsTeams.actions()).inject(constantUsers),
    scenario("Join League Scenario").exec(JoinsLeague.actions()).inject(constantUsers),
    scenario("Make Transfer Scenario").exec(MakesTransfer.actions()).inject(constantUsers),

    // spike
    scenario("User View Spike").exec(ViewsTeams.actions()).inject(spikeInjectionSpec).throttle(spikeThrottle: _*).disablePauses,
    scenario("Join League Spike").exec(JoinsLeague.actions()).inject(spikeInjectionSpec).throttle(spikeThrottle: _*).disablePauses,
    scenario("Make Transfer Spike").exec(MakesTransfer.actions()).inject(spikeInjectionSpec).throttle(spikeThrottle: _*).disablePauses
  ).protocols(Http.protocol)
}