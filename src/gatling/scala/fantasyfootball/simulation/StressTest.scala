package fantasyfootball.simulation

import fantasyfootball.journey.{JoinsLeague, MakesTransfer, ViewsTeams}
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

import scala.concurrent.duration._

/**
  * !!! THIS TEST MIGHT NUKE THE SERVER
  *
  * Test that injects users at a high rate to determine max capacity of system.
  *
  * The 3 scenarios generate equal traffic volumes (based on the test plan document
  * activity table in section 4.2).
  *
  * Note: When executing this test it will be necessary to reload the data in the database
  * because the [[JoinsLeague]] chain creates teams and users can only have a limited
  * amount of teams.
  */
class StressTest extends Simulation {
  setUp(
    scenario("User View Scenario").exec(
      ViewsTeams.actions()
    ).inject(constantUsersPerSec(20) during (250 seconds)),

    scenario("Join League Scenario").exec(
      JoinsLeague.actions()
    ).inject(constantUsersPerSec(20) during (250 seconds)),

    scenario("Make Transfer Scenario").exec(
      MakesTransfer.actions()
    ).inject(constantUsersPerSec(20) during (250 seconds))
  ).protocols(Http.protocol)
}