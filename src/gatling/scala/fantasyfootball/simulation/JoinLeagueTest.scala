package fantasyfootball.simulation

import fantasyfootball.journey.JoinsLeague
import io.gatling.core.Predef._

import scala.concurrent.duration._

class JoinLeagueTest extends Simulation {
  val scn = scenario("Join League Scenario")
    .exec(JoinsLeague.actions())
    .inject(constantUsersPerSec(1) during (500 seconds))

  setUp(scn).protocols(Http.protocol)
}

class ValidateJoinLeagueTest extends Simulation {
  val scn = scenario("Validate Join League Scenario")
    .exec(JoinsLeague.actions())
    .inject(atOnceUsers(1))
    .disablePauses

  setUp(scn).protocols(Http.protocol)
}
