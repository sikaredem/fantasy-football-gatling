package fantasyfootball.simulation

import fantasyfootball.journey.MakesTransfer
import io.gatling.core.Predef._

import scala.concurrent.duration._

class MakeTransferTest extends Simulation {
  val scn = scenario("Make Transfer Scenario")
    .exec(MakesTransfer.actions())
    .inject(constantUsersPerSec(1) during (500 seconds))

  setUp(scn).protocols(Http.protocol)
}

class ValidateMakeTransferTest extends Simulation {
  val scn = scenario("Validate Make Transfer Scenario")
    .exec(MakesTransfer.actions())
    .inject(atOnceUsers(1))
    .disablePauses

  setUp(scn).protocols(Http.protocol)
}
