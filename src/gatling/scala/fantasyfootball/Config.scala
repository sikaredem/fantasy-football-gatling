package fantasyfootball

import com.typesafe.config.ConfigFactory

object Config {
  val appConfig = ConfigFactory.load()
}
