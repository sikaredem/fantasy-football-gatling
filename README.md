fantasy-football-perf-tests
===========================

Gatling project to load test the fantasy football API.
 
To get started refer to the Gatling [docs and tutorials](http://gatling.io/#/resources/documentation).

## TODO

We need a script to setup a clean set of the data before executing these tests.
This is because a user can only create 2 teams. JoinLeagueTest creates new teams for a user
which means the test cannot be rerun unless the data is reset.

## Dependencies

Java 8+

Note: gradle will be installed as part of the build process (see below).

## Build

The project uses the [gradle gatling plugin](https://github.com/lkishalmi/gradle-gatling-plugin) to orchestrate test execution

     $ ./gradlew build 

## Running Tests

Use gradle to execute the tests as follows

     $ ./gradlew gatling-<test name>

where <test name> is one of
     fantasyfootball.simulation.ExpectedLoadTest
     fantasyfootball.simulation.JoinLeagueSimulation
     fantasyfootball.simulation.MakeTransferSimulation
     fantasyfootball.simulation.StressTest
     fantasyfootball.simulation.TwiceExpectedLoadTest
     fantasyfootball.simulation.UserViewSimulation
     fantasyfootball.simulation.ValidateAllTest
     fantasyfootball.simulation.ValidateJoinLeagueSimulation
     fantasyfootball.simulation.ValidateMakeTransferSimulation
     fantasyfootball.simulation.ValidateUserViewSimulation
    
Example:
    
    $ ./gradlew gatling-simulation.UserViewSimulation

OR choose from a list of available tests by omitting the test name

    $ ./gradlew gatling-


## User Journeys

These tests consist of 3 user journeys through the application

1. UserView     - read only operations where a user views their teams and dashboard
2. JoinLeague   - a user creates a team and joins a league
3. MakeTransfer - a user makes a transfer for one of their an existing teams

Each user journey has a separate test that can be executed in isolation. In `src/gatling/scala/fantasyfootball/simulation` you will find a separate file for each journey and also spike, stress and load tests.

## Monitoring Tests

Gatling can provide real-time test metrics using the graphite protocol. This will allow you to monitor the test progress without having to wait for the test to finish. 
To view the metrics you will need to use a tool to visualize the graphite metrics. Grafana is a tool that can visualize graphite metrics. Gatling's graphite configuration can be found in `conf/gatling.conf`.
See `http://gatling.io/docs/2.0.3/realtime_monitoring/index.html` for information about configuring graphite.

## Logging

You can configure logging in:

    fantasy-football-perf/src/gatling/resources/conf/logback.xml

To log requests and responses, change the gatling package log level to TRACE.  Note that this setting is not desirable when executing simulations since it will block
the executing thread, however it is useful for debugging responses.

See http://logback.qos.ch/manual/configuration.html for more information

## Install on linux box

There is a sample script to install the project onto a fresh ubuntu 14.04. Adapt this to suit your environment.

     $ ./install-ubuntu.sh

## OS Limits

On linux the simulation will open a lot of files, which usually exceed the default allowed (at least on ubuntu).
To increase the limit, edit `/etc/security/limits.conf` and set the following line

*               hard    nofile             100000

This sets the hard limit of max open files to 100k.

## System Under Test

Staging environment
F5 load balancer in front of two VMs.
VM: 8GB RAM with 4 CPUs.

## Scenario Data

After executing a scenario, it will be necessary to run the reset database script (ask Craig).
This is because some actions cannot be executed multiple times, for example a user can only create
2 teams, so we cannot reuse the same users between tests.

1. UserViewSimulation

This is a read-only simulation. Some of these users have teams associated and some do not.
You can execute this test without resetting the data.
Data: `users_viewteams.csv`.
Manager Pins: 5000-6999

2. MakeTransferSimulation

This simulation requires users to have a team associated already, then the test will simply make a transfer for the given team.
You can execute this test without resetting the data.
Data: `users_maketransfer.csv`
Manager Pins: 7000-8999

3. JoinLeagueSimulation

This simulation creates a new team for a user and joins a league.
The data must be reset to execute this test multiple times.
Data: `users_joinleague.csv`
Manager Pins: 100000-120000

## SkyBet Stub

Install node.js by uploading and running skybet-stub/skybet-stub-install.sh script
Upload these files to the same directory that ran install script (where the node_modules directory is):
  - skybet-stub.js
  - user_data.json
  - skybet-stub-key.pem
  - skybet-stub-cert.pem
The user_data.json file contains the test user Sky ID and usernames mapped to SSO tokens
The file should contain a JSON object containing a list of SSO tokens mapped to skybet user IDs and usernames
e.g { "e73b60a4db67bff0":{"customerid": "99260258","username":"PERFTESTUSER260258"},"f806301011639536":{"customerid": "99260259","username":"PERFTESTUSER260259"} }