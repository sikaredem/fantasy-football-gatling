/*

This script is intended as very lightweight mock of the SkyBet SSO token authentication service.

It expects the same request signature as the real service, and in turn, sends a response in the same
format as the real service.

The SSO token in the request must map to a key in the JSON object in user_data.json for a valid
response to be sent.

*/

// pull in the express and bod-parser modules, these should be installed using npm
var express = require('express');
var https = require('https');
var bodyParser = require('body-parser');
var fs = require("fs");

// set the simulated response time for each request
// the actual average of the SSO API is 329ms and this server takes ~10ms to respond
// so simulate an additional 320ms by default, or use the cli argument
var simulatedResponseTime = parseInt(process.argv[3]) || 320;
var longSimulatedResponseTime = parseInt(process.argv[4]) || 2000;
var reallyLongSimulatedResponseTime = parseInt(process.argv[5]) || 5000;

// Get content from file and parse
var contents = fs.readFileSync("user_data.json");
var users = JSON.parse(contents);

// create an express app instance
var app = express();

// tell the app to use the json body parser so we can decode the request body
app.use(bodyParser.json());

// create a HTTP POST handler for the "/api/identity/sso" route
app.post('/api/identity/sso', function (req, res) {
  // start timer
  var startTime = process.hrtime();

  // check the token is present in request body
  if (!req.body.params.ssoToken) {
    var errMsg = "SSO token missing from request payload";
    console.error("%s Error: %s", new Date().toString(), errMsg);
    return res.status(400).send(errMsg);
  }

  // get the token from the request object
  var ssoToken = req.body.params.ssoToken;

  // check the token is present in user data
  if (!users[ssoToken]) {
    var errMsg = "Could not find user for SSO token " + ssoToken;
    console.error("%s Error: %s", new Date().toString(), errMsg);
    return res.status(500).send(errMsg);
  }

  // get user from user data object
  var user = users[ssoToken];

  // log the received token to node process stdout so we can see the token request was received
  console.log("%s SSO Token %s has matching user", new Date().toString(), ssoToken, user);

  // create an object containing the user data
  var responsePayloadObject = {
    result: {
      success: true,
      data: {
        email: "an.email@nowhere.com",
        firstname: "Perf",
        lastname: "User" + user.username.replace("PERFTESTUSER", ""),
        customerid: user.customerid,
        fullyregistered: "Y",
        canbet: "Y",
        username: user.username
      }
    }
  };

  // simulate a long response time for 20% of requests
  var rand = (Math.random() * 100);

  var waitTime = simulatedResponseTime;
  if (rand < 10) {
    console.log("%s Really long request simulated for token %s", new Date().toString(), ssoToken);
    waitTime = reallyLongSimulatedResponseTime;
  } else if (rand < 20) {
    console.log("%s Long request simulated for token %s", new Date().toString(), ssoToken);
    waitTime = longSimulatedResponseTime;
  } else {
    // add some extra random wait time (20-100ms) to make response time more realistic
    waitTime += rand;
  }

  // seialise the JSON object so it can be returned in the response body
  var responsePayload = JSON.stringify(responsePayloadObject);

  // wrap sending the response in a timeout (non-blocking) to simulate a real world response time
  setTimeout(function() {

    // send response with the JSON string
    res.send(responsePayload);

    // end timer
    var processTime = process.hrtime(startTime)

    console.log(
      "%s response time for token %s was %s seconds ",
      new Date().toString(),
      ssoToken,
      (processTime[1] / 1000000000) + processTime[0]
    );
  }, waitTime);
});

// set the port the server should listen on
var port = parseInt(process.argv[2]) || 3000;

var options = {
  key: fs.readFileSync('./skybet-stub-key.pem'),
  cert: fs.readFileSync('./skybet-stub-cert.pem')
};

// record time server starts so we can simulate spikes in response times every n minutes
var serverStartTimestamp = new Date().getTime();

https.createServer(options, app).listen(443);

// tell the app to listen on the supplied port
/*app.listen(port, function () {
  // log out a message to the node process stdout so we know the server is ready
  console.log('SkyBet SSO mock server listening on port ' + port + ' and ready to accept requests!');
  console.log('The server will simulate an additional ' + simulatedResponseTime + ' milliseconds of response time per request');
});*/
