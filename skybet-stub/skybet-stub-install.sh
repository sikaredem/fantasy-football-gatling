#!/bin/bash

# install apt packages
sudo apt-get update
sudo apt-get install nodejs npm

# install node modules
npm install express body-parser
