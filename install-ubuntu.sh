#!/usr/bin/env bash

# Purpose: Script to install the load test application on a fresh ubuntu 14.04 installation
# Date:    30/06/2016

# install java
sudo apt-get install -y python-software-properties debconf-utils
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
sudo apt-get install -y oracle-java8-installer

# clone the project from github (a deploy key is on this box in .ssh/id_rsa.pub)
git clone git@github.com:tapatron/ff-perf-tests.git
cd ff-perf-tests/
